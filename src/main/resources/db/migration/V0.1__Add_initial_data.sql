-- Users & Roles
-- INSERT INTO ROLES (ID, ROLE) VALUES (1, 'admin'), (2, 'moderator'), (3, 'candidate'), (4, 'hr');

insert into PERSON (PERSON_ID, EMAIL, FNAME, LNAME, PASSWORD, ROLE, SIGN_IN_PROVIDER)
            values (1, 'axel@test.com', 'Axel', 'Green', 'pwd', 'CANDIDATE', 'linkedin');
insert into PERSON (PERSON_ID, EMAIL, FNAME, LNAME, PASSWORD, ROLE, SIGN_IN_PROVIDER)
            values (2, 'hr@icq.com', 'Gerard', 'Berger', 'pwd', 'INTERVIEWER', 'linkedin');
insert into PERSON (PERSON_ID, EMAIL, FNAME, LNAME, PASSWORD, ROLE, SIGN_IN_PROVIDER)
            values (3, 'admin@test.com', 'Admin', 'Admin', 'pwd', 'ADMIN', 'linkedin');
insert into PERSON (PERSON_ID, EMAIL, FNAME, LNAME, PASSWORD, ROLE, SIGN_IN_PROVIDER)
            values (4, 'jack@test.com', 'Jack', 'Bauer', 'pwd', 'CANDIDATE', 'linkedin');

insert into CANDIDATE (PERSON_ID)
            values (1);
insert into CANDIDATE (PERSON_ID)
            values (4);
insert into INTERVIEWER (PERSON_ID, COMPANY)
            values (2, "ICQ");

-- INSERT INTO PERSON_ROLES (PERSON_ID, ROLE_ID) VALUES (1, 3), (2, 4), (3, 1);
