package com.vreality.model.actors.authentication;

import com.vreality.model.interview.Person;

public class AccountUtil{
    public static Account getAccountFromPerson(Person person){
        return new Account(person.getEmail(), person.getPassword(), person.getFirstName(), person.getLastName());
    }
}
