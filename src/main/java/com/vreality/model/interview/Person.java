package com.vreality.model.interview;

import com.vreality.model.actors.Actor;
import com.vreality.model.actors.Role;
import com.vreality.model.actors.authentication.SocialProvider;

import javax.persistence.*;

/**
 * Date: 4/22/14
 * Time: 5:36 PM
 */
@Entity
@Table( name = "PERSON")
@Inheritance(strategy= InheritanceType.JOINED)
public class Person implements Actor{
    @Id
    @GeneratedValue
    @Column(name = "PERSON_ID")
    private long id;

    @Column(name = "EMAIL", unique = true)
    private String email;
    @Column(name = "FNAME")
    private String firstName;
    @Column(name = "LNAME")
    private String lastName;
    @Column(name = "PASSWORD", length = 255)
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE", length = 20, nullable = false)
    private Role role;
    @Enumerated(EnumType.STRING)
    @Column(name = "SIGN_IN_PROVIDER", length = 20)
    private SocialProvider signInProvider;
    @Column(name = "SIGN_IN_PROVIDER_USER_ID")
    private String providerUserId;

    protected Person(String email, String firstName, String lastName, Role role) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    protected Person(String email, String firstName, String lastName, Role role, SocialProvider signInProvider, String providerUserId) {
        this(email, firstName, lastName, role);
        this.signInProvider = signInProvider;
        this.providerUserId = providerUserId;
    }

    public Person() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String fname) {
        this.firstName = fname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lname) {
        this.lastName = lname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public SocialProvider getSignInProvider() {
        return signInProvider;
    }

    public void setSignInProvider(SocialProvider signInProvider) {
        this.signInProvider = signInProvider;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }
}
