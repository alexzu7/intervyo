package com.vreality.dao;

import com.vreality.model.actors.authentication.Account;
import com.vreality.model.interview.Person;

/**
 * Created by noam - 8/13/14
 */

public interface PersonDao{
    void savePerson(Person person);
    Person loadPerson(Class clazz, Long id);
    Account findAccountByUsername(String username);
    Person findPersonByUsername(String username);
}