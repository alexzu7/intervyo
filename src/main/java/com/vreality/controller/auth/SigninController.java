package com.vreality.controller.auth;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by noam - 8/15/14
 */

@Controller
public class SigninController {

    @RequestMapping(value="/signin", method=RequestMethod.GET)
    public void signin() {
    }
}