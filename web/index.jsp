<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv='x-dns-prefetch-control' content='on'>
    <link rel='dns-prefetch' href='https://intervyo.s3.amazonaws.com'>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="css/layout.css" type="text/css" media="all"/>
    <title><spring:message code="product.name"/></title>
</head>
<body class="demo">
    <header>
        <a href="<spring:url value="/"/>" id="logo"><img src="images/common/logo_intervyo.jpg" width="179" height="73"/></a>
        <c:choose>
        <c:when test="${pageContext['request'].userPrincipal == null}">
            <ul>
                <li><a href="<spring:url value="/signin"/>">Login</a></li>
            </ul>
        </c:when>
        <c:otherwise>
            <div id="welcome">Welcome ${pageContext.request.userPrincipal.name}
                | <a href="<spring:url value="/signout"/>">Logout</a>
            </div>
        </c:otherwise>
        </c:choose>
        <div id="clock"></div>
    </header>
    <main>
        <h1>Welcome to Admin System.</h1>
    </main>

    <% /********* COMMON ************/ %>
    <!-- JQuery and Bootstrap Javascript files -->
    <%--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--%>
    <%--<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>--%>
    <script type="text/javascript" src="js/common/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/common/utils.js"></script>
    <script type="text/javascript" src="js/common/EventManager.js"></script>
    <script type="text/javascript" src="js/common/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="js/common/ba-debug.min.js"></script>
    <script type="text/javascript" src="js/video/jquery.html5Loader.min.js"></script>
    <%--<script type="text/javascript" src="js/video/jquery.html5Loader.js"></script>--%>
    <script type="text/javascript" src="js/video/screenfull.js"></script>

    <script type="text/javascript">
        var gContextPath = "${pageContext.request.contextPath}";
    </script>
</body>

</html>