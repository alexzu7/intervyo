<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/spring-social/social/tags" prefix="facebook" %>
<%@ page session="false" %>

<head>
    <meta http-equiv='x-dns-prefetch-control' content='on'>
    <link rel='dns-prefetch' href='https://intervyo.s3.amazonaws.com'>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="<c:url value="/css/layout.css"/>" type="text/css" media="all"/>
    <title><spring:message code="product.name"/></title>
</head>

<form id="signin" action="<c:url value="/signin/authenticate" />" method="post">
    <div class="formInfo">
        <c:if test="${param.error eq 'bad_credentials'}">
            <div class="error">
                Your sign in information was incorrect.
                Please try again or <a href="<c:url value="/signup" />">sign up</a>.
            </div>
        </c:if>
        <c:if test="${param.error eq 'multiple_users'}">
            <div class="error">
                Multiple local accounts are connected to the provider account.
                Try again with a different provider or with your username and password.
            </div>
        </c:if>
    </div>
    <fieldset>
        <label for="login">Username</label>
        <input id="login" name="j_username" type="text" size="25" <c:if test="${not empty signinErrorMessage}">value="${SPRING_SECURITY_LAST_USERNAME}"</c:if> />
        <label for="password">Password</label>
        <input id="password" name="j_password" type="password" size="25" />
        <button type="submit">Sign In</button>
    </fieldset>


    <p>Or you can <a href="<c:url value="/signup"/>">signup</a> with a new account.</p>
</form>

<hr/>

<!-- LINKEDIN SIGNIN -->
<form name="li_signin" id="li_signin" action="<c:url value="/signin/linkedin"/>" method="POST">
    <input type="image" src="<c:url value="/images/auth/sign-in-with-linkedin.png"/>" width="244" />
</form>

<!-- FACEBOOK SIGNIN -->
<form name="fb_signin" id="fb_signin" action="<c:url value="/signin/facebook"/>" method="POST">
    <input type="hidden" name="scope" value="publish_stream,user_photos,offline_access" />
    <input type="image" src="<c:url value="/images/auth/sign-in-with-facebook.png"/>" width="244" />
</form>

<!-- TWITTER SIGNIN -->
<form id="tw_signin" action="<c:url value="/signin/twitter"/>" method="POST">
    <input type="image" src="<c:url value="/images/auth/sign-in-with-twitter.png"/>" width="244" />
</form>

